package labcodeinspection;

import java.util.Scanner;
final class EmailApp{
	
	private EmailApp() {
	}

	/**
	 * 
	 * @return
	 */
	public static void main(String[] args) {
		
		final Scanner scInput = new Scanner(System.in);

		System.out.print("Enter your first name: ");
		final String firstName = scInput.nextLine();

		System.out.print("Enter your last name: ");
		final String lastName = scInput.nextLine();

		System.out.print("\nDEPARTMENT CODE\n1. for sales\n2. for Development\n3. for accounting\nEnter code: ");

		final int depChoice = scInput.nextInt();
		scInput.close();

		final Email email = new Email(firstName, lastName);
		email.setDeparment(depChoice);
		email.generateEmail();
		email.showInfo();
		

	}
}
